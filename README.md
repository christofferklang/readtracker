# ReadTracker

An app for keeping track on the books you've read, and give you insight into your reading patterns.

Published on [Google Play](https://play.google.com/store/apps/details?id=com.readtracker).

## Note

This app has been in development for a few years a side project of mine for learning Android, so the code definitely has it's warts here and there. Pull request or suggestions for code clean-ups greatly appreciated.