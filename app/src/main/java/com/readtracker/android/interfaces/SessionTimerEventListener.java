package com.readtracker.android.interfaces;

public interface SessionTimerEventListener {
  public void onStarted();
  public void onStopped();
}
